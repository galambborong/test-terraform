terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "linode" {
  token = var.linode_token
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}

# create nanode

resource "linode_instance" "test-terraform-instance" {
  label     = "test-terraform-label"
  image     = "linode/ubuntu20.04"
  region    = "us-central"
  type      = "g6-nanode-1"
  root_pass = var.linode_instance_root_password

  provisioner "file" {
    source      = "sample.sh"
    destination = "/tmp/sample.sh"
    connection {
      type     = "ssh"
      host     = self.ip_address
      user     = "root"
      password = var.linode_instance_root_password
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/sample.sh",
      "/tmp/sample.sh",
      "sleep 1"
    ]
    connection {
      type     = "ssh"
      host     = self.ip_address
      user     = "root"
      password = var.linode_instance_root_password
    }
  }
}

# update cloudflare dns record with ip address of new nanode

resource "cloudflare_record" "test-update-cf-dns-record" {
  zone_id = var.cloudflare_zone_id
  name    = var.domain_name
  type    = "A"
  value   = linode_instance.test-terraform-instance.ip_address
  proxied = true
}

# variables

variable "linode_token" {}
variable "linode_instance_root_password" {}
variable "cloudflare_token" {}
variable "cloudflare_zone_id" {}
variable "domain_name" {}
